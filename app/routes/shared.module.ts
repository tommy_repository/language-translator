/**
 * Created by tomislavfabeta on 5/3/17.
 */
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {MaterialModule} from "@angular/material";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import 'hammerjs/hammer';
@NgModule({
    exports: [
        MaterialModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    // schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class SharedModule{}