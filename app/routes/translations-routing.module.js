"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var router_1 = require("@angular/router");
var core_1 = require("@angular/core");
var translation_list_component_1 = require("../components/translation/translation-list.component");
var translation_view_component_1 = require("../components/translation/translation-view.component");
var translation_update_component_1 = require("../components/translation/translation-update.component");
var shared_module_1 = require("./shared.module");
var routes = [
    {
        path: '',
        component: translation_list_component_1.TranslationListComponent,
        data: { preload: false }
    },
    {
        path: 'view/:id',
        component: translation_view_component_1.TranslationViewComponent,
        data: { preload: false }
    },
    {
        path: 'update/:id',
        component: translation_update_component_1.TranslationUpdateComponent,
        data: { preload: true }
    }
];
var TranslationsRoutingModule = (function () {
    function TranslationsRoutingModule() {
    }
    TranslationsRoutingModule = __decorate([
        core_1.NgModule({
            declarations: [
                translation_list_component_1.TranslationListComponent,
                translation_view_component_1.TranslationViewComponent,
                translation_update_component_1.TranslationUpdateComponent
            ],
            imports: [
                shared_module_1.SharedModule,
                router_1.RouterModule.forChild(routes)
            ],
            exports: [
                router_1.RouterModule
            ]
        })
    ], TranslationsRoutingModule);
    return TranslationsRoutingModule;
}());
exports.TranslationsRoutingModule = TranslationsRoutingModule;
//# sourceMappingURL=translations-routing.module.js.map