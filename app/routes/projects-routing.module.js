"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var router_1 = require("@angular/router");
var project_list_component_1 = require("../components/project/project-list.component");
var project_view_component_1 = require("../components/project/project-view.component");
var project_update_component_1 = require("../components/project/project-update.component");
var core_1 = require("@angular/core");
var shared_module_1 = require("./shared.module");
var routes = [
    {
        path: '',
        component: project_list_component_1.ProjectListComponent,
        data: { preload: false }
    },
    {
        path: 'list',
        component: project_list_component_1.ProjectListComponent,
        data: { preload: false }
    },
    {
        path: 'view/:id',
        component: project_view_component_1.ProjectViewComponent,
        data: { preload: false }
    },
    {
        path: 'update/:id',
        component: project_update_component_1.ProjectUpdateComponent,
        data: { preload: true }
    }
];
var ProjectsRoutingModule = (function () {
    function ProjectsRoutingModule() {
    }
    ProjectsRoutingModule = __decorate([
        core_1.NgModule({
            declarations: [
                project_list_component_1.ProjectListComponent,
                project_view_component_1.ProjectViewComponent,
                project_update_component_1.ProjectUpdateComponent
            ],
            imports: [
                shared_module_1.SharedModule,
                router_1.RouterModule.forChild(routes)
            ],
            exports: [
                router_1.RouterModule
            ],
            providers: []
        })
    ], ProjectsRoutingModule);
    return ProjectsRoutingModule;
}());
exports.ProjectsRoutingModule = ProjectsRoutingModule;
//# sourceMappingURL=projects-routing.module.js.map