import {Routes, RouterModule} from "@angular/router";
import {NgModule} from "@angular/core";
import {TranslationListComponent} from "../components/translation/translation-list.component";
import {TranslationViewComponent} from "../components/translation/translation-view.component";
import {TranslationUpdateComponent} from "../components/translation/translation-update.component";
import {SharedModule} from "./shared.module";
const routes: Routes = [
    {
        path: '',
        component: TranslationListComponent,
        data: { preload: false }
    },
    {
        path: 'view/:id',
        component: TranslationViewComponent,
        data: { preload: false }
    },
    {
        path: 'update/:id',
        component: TranslationUpdateComponent,
        data: { preload: true }
    }
];

@NgModule({
    declarations: [
        TranslationListComponent,
        TranslationViewComponent,
        TranslationUpdateComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TranslationsRoutingModule{}