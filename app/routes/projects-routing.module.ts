import {Routes, RouterModule} from "@angular/router";
import {ProjectListComponent} from "../components/project/project-list.component";
import {ProjectViewComponent} from "../components/project/project-view.component";
import {ProjectUpdateComponent} from "../components/project/project-update.component";
import {NgModule} from "@angular/core";
import {ProjectApiService} from "../services/project.api.service";
import {SharedModule} from "./shared.module";
const routes: Routes = [
    {
        path: '',
        component: ProjectListComponent,
        data: { preload: false }
    },
    {
        path: 'list',
        component: ProjectListComponent,
        data: { preload: false }
    },
    {
        path: 'view/:id',
        component: ProjectViewComponent,
        data: { preload: false }
    },
    {
        path: 'update/:id',
        component: ProjectUpdateComponent,
        data: { preload: true }
    }
];

@NgModule({
    declarations: [
        ProjectListComponent,
        ProjectViewComponent,
        ProjectUpdateComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
    providers: [
    ]
})
export class ProjectsRoutingModule{}