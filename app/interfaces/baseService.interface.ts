/**
 * Created by tomislavfabeta on 21/2/17.
 */
interface BaseServiceInterface{
    /**
     * Api calls
     */
    getById(model);
    getAll();
    create(model);
    update(model);
    remove(model);

}