/**
 * Created by tomislavfabeta on 21/2/17.
 */
interface ServiceInterface{
    /**
     * Returns api model call
     * It is used like api/v1/${apiModel()}
     */
    apiModel():string;
}