
import {Component} from "@angular/core";
@Component({
    selector: 'content',
    templateUrl: 'app/templates/layout/content.template.html'
})
export class ContentComponent{}