import {Component} from "@angular/core";
@Component({
    selector: 'translation-view',
    templateUrl: 'app/templates/translation/view.template.html',
})
export class TranslationViewComponent{}