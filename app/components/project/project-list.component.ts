import {Component, Inject, Input} from "@angular/core";
import {Project} from "../../models/Project";
import {ProjectApiService} from "../../services/project.api.service";
import {Observable} from "rxjs/rx";
@Component({
    selector: 'project-list',
    templateUrl: 'app/templates/project/list.template.html',
})
export class ProjectListComponent{
    @Input()
    projects: Project[] = [];

    constructor(@Inject(ProjectApiService) private projectApiService: ProjectApiService) { }

    ngOnInit() {
        this.getProjects();
        let testProject = new Project();
        testProject.id = 1;
        testProject.name = 'Project 1';
        testProject.description = 'Some description';
        testProject.active = 1;
        this.projects.push(testProject);

        let testProject3 = new Project();
        testProject3.id = 3;
        testProject3.name = 'Project 3';
        testProject3.description = 'Some description for project 3';
        testProject3.active = 0;
        this.projects.push(testProject3);

        let testProject2 = new Project();
        testProject2.id = 2;
        testProject2.name = 'Project 2';
        testProject2.description = 'Some description for project 2';
        testProject2.active = 0;
        this.projects.push(testProject2);

        this.orderList('id', 'desc');
    }

    private getProjects() {
        let projects: Project[] = [];
        let self = this;
        this.projectApiService.getAll().subscribe(
            data => {
                this.projects = data;
                return true;
            },
            error => {
                console.error('Error geting projects!');
                return Observable.throw(error);
            }
        );
    }

    public orderList(attribute: string, type: string = 'asc') {
        this.projects.sort((a: Project, b: Project) =>{
            if (type == 'desc'){
                if (a[attribute] < b[attribute]){
                    return 1;
                }else if (a[attribute] > b[attribute]){
                    return -1;
                }
            }else {
                if (a[attribute] > b[attribute]){
                    return 1;
                }else if (a[attribute] < b[attribute]){
                    return -1;
                }
            }


            return 0;
        });
    }
}