"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var Project_1 = require("../../models/Project");
var project_api_service_1 = require("../../services/project.api.service");
var rx_1 = require("rxjs/rx");
var ProjectListComponent = (function () {
    function ProjectListComponent(projectApiService) {
        this.projectApiService = projectApiService;
        this.projects = [];
    }
    ProjectListComponent.prototype.ngOnInit = function () {
        this.getProjects();
        var testProject = new Project_1.Project();
        testProject.id = 1;
        testProject.name = 'Project 1';
        testProject.description = 'Some description';
        testProject.active = 1;
        this.projects.push(testProject);
        var testProject3 = new Project_1.Project();
        testProject3.id = 3;
        testProject3.name = 'Project 3';
        testProject3.description = 'Some description for project 3';
        testProject3.active = 0;
        this.projects.push(testProject3);
        var testProject2 = new Project_1.Project();
        testProject2.id = 2;
        testProject2.name = 'Project 2';
        testProject2.description = 'Some description for project 2';
        testProject2.active = 0;
        this.projects.push(testProject2);
        this.orderList('id', 'desc');
    };
    ProjectListComponent.prototype.getProjects = function () {
        var _this = this;
        var projects = [];
        var self = this;
        this.projectApiService.getAll().subscribe(function (data) {
            _this.projects = data;
            return true;
        }, function (error) {
            console.error('Error geting projects!');
            return rx_1.Observable.throw(error);
        });
    };
    ProjectListComponent.prototype.orderList = function (attribute, type) {
        if (type === void 0) { type = 'asc'; }
        this.projects.sort(function (a, b) {
            if (type == 'desc') {
                if (a[attribute] < b[attribute]) {
                    return 1;
                }
                else if (a[attribute] > b[attribute]) {
                    return -1;
                }
            }
            else {
                if (a[attribute] > b[attribute]) {
                    return 1;
                }
                else if (a[attribute] < b[attribute]) {
                    return -1;
                }
            }
            return 0;
        });
    };
    __decorate([
        core_1.Input()
    ], ProjectListComponent.prototype, "projects", void 0);
    ProjectListComponent = __decorate([
        core_1.Component({
            selector: 'project-list',
            templateUrl: 'app/templates/project/list.template.html',
        }),
        __param(0, core_1.Inject(project_api_service_1.ProjectApiService))
    ], ProjectListComponent);
    return ProjectListComponent;
}());
exports.ProjectListComponent = ProjectListComponent;
//# sourceMappingURL=project-list.component.js.map