/**
 * Created by tomislavfabeta on 12/2/17.
 */
import {Component} from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: 'app/templates/app.template.html'
})
export class AppComponent {
}
