import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'data-grid',
    templateUrl: 'app/templates/project/common/grid/grid.component.html'
})
export class DataGridComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

}