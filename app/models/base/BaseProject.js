"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var BaseProject = (function (_super) {
    __extends(BaseProject, _super);
    function BaseProject() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(BaseProject.prototype, "languages", {
        get: function () {
            return this._languages;
        },
        set: function (value) {
            this._languages = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseProject.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (value) {
            this._active = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseProject.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (value) {
            this._description = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseProject.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseProject.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    return BaseProject;
}(BaseModel_1.BaseModel));
exports.BaseProject = BaseProject;
//# sourceMappingURL=BaseProject.js.map