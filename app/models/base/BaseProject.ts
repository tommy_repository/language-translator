import {BaseModel} from "./BaseModel";
import {Language} from "../Language";
export class BaseProject extends BaseModel{
    get languages(): Language[] {
        return this._languages;
    }

    set languages(value: Language[]) {
        this._languages = value;
    }
    get active(): number {
        return this._active;
    }

    set active(value: number) {
        this._active = value;
    }
    get description(): string {
        return this._description;
    }

    set description(value: string) {
        this._description = value;
    }
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }
    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
    private _id:number;
    private _name:string;
    private _description:string;
    private _active:number;
    private _languages: Language[];
}