import {BaseModel} from "./BaseModel";
export class BaseTranslation extends BaseModel{
    get translation(): string {
        return this._translation;
    }

    set translation(value: string) {
        this._translation = value;
    }
    get underscoredKeyword(): string {
        return this._underscoredKeyword;
    }

    set underscoredKeyword(value: string) {
        this._underscoredKeyword = value;
    }
    get keyword(): string {
        return this._keyword;
    }

    set keyword(value: string) {
        this._keyword = value;
    }
    get languageId(): number {
        return this._languageId;
    }

    set languageId(value: number) {
        this._languageId = value;
    }
    get projectId(): number {
        return this._projectId;
    }

    set projectId(value: number) {
        this._projectId = value;
    }
    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
    private _id:number;
    private _projectId:number;
    private _languageId:number;
    private _keyword:string;
    private _underscoredKeyword:string;
    private _translation:string;
}