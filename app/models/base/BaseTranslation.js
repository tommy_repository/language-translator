"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var BaseTranslation = (function (_super) {
    __extends(BaseTranslation, _super);
    function BaseTranslation() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(BaseTranslation.prototype, "translation", {
        get: function () {
            return this._translation;
        },
        set: function (value) {
            this._translation = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseTranslation.prototype, "underscoredKeyword", {
        get: function () {
            return this._underscoredKeyword;
        },
        set: function (value) {
            this._underscoredKeyword = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseTranslation.prototype, "keyword", {
        get: function () {
            return this._keyword;
        },
        set: function (value) {
            this._keyword = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseTranslation.prototype, "languageId", {
        get: function () {
            return this._languageId;
        },
        set: function (value) {
            this._languageId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseTranslation.prototype, "projectId", {
        get: function () {
            return this._projectId;
        },
        set: function (value) {
            this._projectId = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseTranslation.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    return BaseTranslation;
}(BaseModel_1.BaseModel));
exports.BaseTranslation = BaseTranslation;
//# sourceMappingURL=BaseTranslation.js.map