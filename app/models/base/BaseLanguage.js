"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseModel_1 = require("./BaseModel");
var BaseLanguage = (function (_super) {
    __extends(BaseLanguage, _super);
    function BaseLanguage() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(BaseLanguage.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (value) {
            this._active = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseLanguage.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseLanguage.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (value) {
            this._id = value;
        },
        enumerable: true,
        configurable: true
    });
    return BaseLanguage;
}(BaseModel_1.BaseModel));
exports.BaseLanguage = BaseLanguage;
//# sourceMappingURL=BaseLanguage.js.map