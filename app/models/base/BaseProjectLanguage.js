"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseLanguage_1 = require("./BaseLanguage");
var BaseProjectLanguage = (function (_super) {
    __extends(BaseProjectLanguage, _super);
    function BaseProjectLanguage() {
        _super.apply(this, arguments);
    }
    Object.defineProperty(BaseProjectLanguage.prototype, "translations", {
        get: function () {
            return this._translations;
        },
        set: function (value) {
            this._translations = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(BaseProjectLanguage.prototype, "projectId", {
        get: function () {
            return this._projectId;
        },
        set: function (value) {
            this._projectId = value;
        },
        enumerable: true,
        configurable: true
    });
    return BaseProjectLanguage;
}(BaseLanguage_1.BaseLanguage));
exports.BaseProjectLanguage = BaseProjectLanguage;
//# sourceMappingURL=BaseProjectLanguage.js.map