import {Translation} from "../Translation";
import {BaseLanguage} from "./BaseLanguage";
export class BaseProjectLanguage extends BaseLanguage{
    get translations(): Translation[] {
        return this._translations;
    }

    set translations(value: Translation[]) {
        this._translations = value;
    }
    get projectId(): number {
        return this._projectId;
    }

    set projectId(value: number) {
        this._projectId = value;
    }

    private _projectId:number;
    private _translations: Translation[];
}