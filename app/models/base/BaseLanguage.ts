import {BaseModel} from "./BaseModel";
import {Translation} from "../Translation";
export class BaseLanguage extends BaseModel{
    get active(): number {
        return this._active;
    }

    set active(value: number) {
        this._active = value;
    }
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }
    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }
    private _id:number;
    private _name:string;
    private _active:number;
}