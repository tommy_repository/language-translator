"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseTranslation_1 = require("./base/BaseTranslation");
var Translation = (function (_super) {
    __extends(Translation, _super);
    function Translation() {
        _super.apply(this, arguments);
    }
    return Translation;
}(BaseTranslation_1.BaseTranslation));
exports.Translation = Translation;
//# sourceMappingURL=Translation.js.map