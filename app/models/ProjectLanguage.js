"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseProjectLanguage_1 = require("./base/BaseProjectLanguage");
var ProjectLanguage = (function (_super) {
    __extends(ProjectLanguage, _super);
    function ProjectLanguage() {
        _super.apply(this, arguments);
    }
    return ProjectLanguage;
}(BaseProjectLanguage_1.BaseProjectLanguage));
exports.ProjectLanguage = ProjectLanguage;
//# sourceMappingURL=ProjectLanguage.js.map