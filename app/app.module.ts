/**
 * Created by tomislavfabeta on 12/2/17.
 */
import {NgModule, CUSTOM_ELEMENTS_SCHEMA}      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule, JsonpModule} from '@angular/http';
import {AppComponent} from "./components/app.component";
import {HeaderComponent} from "./components/layout/header.component";
import {AppRoutingModule} from "./app-routing.module";
import {ContentComponent} from "./components/layout/content.component";
import {ProjectApiService} from "./services/project.api.service";
import {MaterialModule, MdRippleModule} from "@angular/material";
// import 'hammerjs/hammerjs';

@NgModule({
    imports:      [
        MaterialModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        JsonpModule,
        AppRoutingModule,
    ],
    exports: [
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        ContentComponent,
    ],
    providers: [
        ProjectApiService
    ],
    // exports: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap:    [ AppComponent]
})
export class AppModule { }
