import {Routes, RouterModule} from "@angular/router";
import {HeaderComponent} from "./components/layout/header.component";
import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {SelectivePreloadingStrategy} from "./selective-preloading-strategy";
import {MaterialModule} from "@angular/material";
const routes: Routes = [
    {
        path: '', redirectTo: 'projects',
        pathMatch: 'full',
        data: { preload: false }
    },
    {
        path: 'projects',
        loadChildren: 'app/routes/projects-routing.module#ProjectsRoutingModule'
    },
    {
        path: 'translations',
        loadChildren: 'app/routes/translations-routing.module#TranslationsRoutingModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
        routes,
        { preloadingStrategy: SelectivePreloadingStrategy }
        ),

    ],
    exports: [
        RouterModule,
    ],
    providers: [
        SelectivePreloadingStrategy
    ],
    // schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class AppRoutingModule {}