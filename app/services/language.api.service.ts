import {Injectable} from "@angular/core";
import {BaseApiService} from "./base/base.api.service";
import {Response} from "@angular/http";
import {Language} from "../models/Language";
@Injectable()
export class LanguageApiService extends BaseApiService implements ServiceInterface{
    /**
     * @param response
     * @return {Language[]}
     */
    protected onResponse(response: Response): Language[] {
        let languages: Language[] = [];
        for (let key in response){
            var row = response[key];
            let language = new Language();

            language.id = row.id;
            language.active = row.active;
            language.name = row.name;
            languages.push(language);
        }

        return languages;
    }

    /**
     * @return {string}
     */
    apiModel(): string {
        return 'language';
    }
}
