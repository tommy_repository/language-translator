"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var base_api_service_1 = require("./base/base.api.service");
var Project_1 = require("../models/Project");
var ProjectApiService = (function (_super) {
    __extends(ProjectApiService, _super);
    function ProjectApiService() {
        _super.apply(this, arguments);
    }
    /**
     * @param response
     * @return {Project[]}
     */
    ProjectApiService.prototype.onResponse = function (response) {
        var projects = [];
        for (var key in response) {
            var row = response[key];
            var project = new Project_1.Project();
            project.id = row.id;
            project.active = row.active;
            project.name = row.name;
            project.description = row.decription;
            project.languages = row.languages;
            projects.push(project);
        }
        return projects;
    };
    /**
     * @return {string}
     */
    ProjectApiService.prototype.apiModel = function () {
        return 'project';
    };
    ProjectApiService = __decorate([
        core_1.Injectable()
    ], ProjectApiService);
    return ProjectApiService;
}(base_api_service_1.BaseApiService));
exports.ProjectApiService = ProjectApiService;
//# sourceMappingURL=project.api.service.js.map