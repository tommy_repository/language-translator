import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {BaseApiService} from "./base/base.api.service";
import {Translation} from "../models/Translation";
@Injectable()
export class TranslationApiService extends BaseApiService implements ServiceInterface{
    /**
     * @param response
     * @return {Translation[]}
     */
    protected onResponse(response: Response): Translation[] {
        let translations: Translation[] = [];
        for (let key in response){
            var row = response[key];
            let translation = new Translation();

            translation.id = row.id;
            translation.keyword = row.keyword;
            translation.underscoredKeyword = row.underscoredKeyword;
            translation.projectId = row.projectId;
            translation.translation = row.translation;

            translations.push(translation);
        }

        return translations;
    }

    /**
     * @return {string}
     */
    apiModel(): string {
        return 'translation';
    }
}
