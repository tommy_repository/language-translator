"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var base_api_service_1 = require("./base/base.api.service");
var ProjectLanguage_1 = require("../models/ProjectLanguage");
var ProjectLanguageApiService = (function (_super) {
    __extends(ProjectLanguageApiService, _super);
    function ProjectLanguageApiService() {
        _super.apply(this, arguments);
    }
    /**
     * @param response
     * @return {ProjectLanguage[]}
     */
    ProjectLanguageApiService.prototype.onResponse = function (response) {
        var projectLanguages = [];
        for (var key in response) {
            var row = response[key];
            var projectLanguage = new ProjectLanguage_1.ProjectLanguage();
            projectLanguage.id = row.id;
            projectLanguage.active = row.active;
            projectLanguage.name = row.name;
            projectLanguage.projectId = row.projectId;
            projectLanguage.translations = row.translations;
            projectLanguages.push(projectLanguage);
        }
        return projectLanguages;
    };
    /**
     * @return {string}
     */
    ProjectLanguageApiService.prototype.apiModel = function () {
        return 'language';
    };
    ProjectLanguageApiService = __decorate([
        core_1.Injectable()
    ], ProjectLanguageApiService);
    return ProjectLanguageApiService;
}(base_api_service_1.BaseApiService));
exports.ProjectLanguageApiService = ProjectLanguageApiService;
//# sourceMappingURL=projectLanguage.api.service.js.map