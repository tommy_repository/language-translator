"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var base_api_service_1 = require("./base/base.api.service");
var Translation_1 = require("../models/Translation");
var TranslationApiService = (function (_super) {
    __extends(TranslationApiService, _super);
    function TranslationApiService() {
        _super.apply(this, arguments);
    }
    /**
     * @param response
     * @return {Translation[]}
     */
    TranslationApiService.prototype.onResponse = function (response) {
        var translations = [];
        for (var key in response) {
            var row = response[key];
            var translation = new Translation_1.Translation();
            translation.id = row.id;
            translation.keyword = row.keyword;
            translation.underscoredKeyword = row.underscoredKeyword;
            translation.projectId = row.projectId;
            translation.translation = row.translation;
            translations.push(translation);
        }
        return translations;
    };
    /**
     * @return {string}
     */
    TranslationApiService.prototype.apiModel = function () {
        return 'translation';
    };
    TranslationApiService = __decorate([
        core_1.Injectable()
    ], TranslationApiService);
    return TranslationApiService;
}(base_api_service_1.BaseApiService));
exports.TranslationApiService = TranslationApiService;
//# sourceMappingURL=translation.api.service.js.map