import {Injectable} from "@angular/core";
import {BaseApiService} from "./base/base.api.service";
import {Response} from "@angular/http";
import {Project} from "../models/Project";
@Injectable()
export class ProjectApiService extends BaseApiService implements ServiceInterface{

    /**
     * @param response
     * @return {Project[]}
     */
    protected onResponse(response: Response): Project[] {
        let projects: Project[] = [];
        for (let key in response){
            let row = response[key];
            let project = new Project();

            project.id = row.id;
            project.active = row.active;
            project.name = row.name;
            project.description = row.decription;
            project.languages = row.languages;

            projects.push(project);
        }

        return projects;
    }

    /**
     * @return {string}
     */
    apiModel(): string {
        return 'project';
    }
}

