import {Injectable} from "@angular/core";
import {BaseApiService} from "./base/base.api.service";
import {Response} from "@angular/http";
import {ProjectLanguage} from "../models/ProjectLanguage";
@Injectable()
export class ProjectLanguageApiService extends BaseApiService implements ServiceInterface{
    /**
     * @param response
     * @return {ProjectLanguage[]}
     */
    protected onResponse(response: Response): ProjectLanguage[] {
        let projectLanguages: ProjectLanguage[] = [];
        for (let key in response){
            var row = response[key];
            let projectLanguage = new ProjectLanguage();

            projectLanguage.id = row.id;
            projectLanguage.active = row.active;
            projectLanguage.name = row.name;
            projectLanguage.projectId = row.projectId;
            projectLanguage.translations = row.translations;

            projectLanguages.push(projectLanguage);
        }

        return projectLanguages;
    }

    /**
     * @return {string}
     */
    apiModel(): string {
        return 'language';
    }
}
