"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var base_api_service_1 = require("./base/base.api.service");
var Language_1 = require("../models/Language");
var LanguageApiService = (function (_super) {
    __extends(LanguageApiService, _super);
    function LanguageApiService() {
        _super.apply(this, arguments);
    }
    /**
     * @param response
     * @return {Language[]}
     */
    LanguageApiService.prototype.onResponse = function (response) {
        var languages = [];
        for (var key in response) {
            var row = response[key];
            var language = new Language_1.Language();
            language.id = row.id;
            language.active = row.active;
            language.name = row.name;
            languages.push(language);
        }
        return languages;
    };
    /**
     * @return {string}
     */
    LanguageApiService.prototype.apiModel = function () {
        return 'language';
    };
    LanguageApiService = __decorate([
        core_1.Injectable()
    ], LanguageApiService);
    return LanguageApiService;
}(base_api_service_1.BaseApiService));
exports.LanguageApiService = LanguageApiService;
//# sourceMappingURL=language.api.service.js.map