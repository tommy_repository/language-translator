///<reference path="../../../node_modules/rxjs/add/operator/map.d.ts"/>
///<reference path="../../interfaces/baseService.interface.ts"/>
/**
 * Created by tomislavfabeta on 12/2/17.
 */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var http_1 = require('@angular/http');
var rx_1 = require('rxjs/rx');
require('rxjs/add/operator/map');
var core_1 = require("@angular/core");
var BaseApiService = (function () {
    /**
     * @param http
     */
    function BaseApiService(http) {
        this.http = http;
        /**
         *
         * @type {string}
         */
        this.CREATE = '';
        this.GET = '';
        this.UPDATE = '';
        this.DELETE = '';
    }
    /**
     * Extend this method to change api version
     * @return {string}
     */
    BaseApiService.prototype.apiVersion = function () {
        return 'v1/';
    };
    /**
     * Extend this method to change base api url
     * @return {string}
     */
    BaseApiService.prototype.apiBaseUrl = function () {
        return "some.api.com/" + this.apiVersion();
    };
    /**
     *
     * @param model
     * @return {Observable<R>}
     */
    BaseApiService.prototype.getById = function (model) {
        console.log(model);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return this.http.get(this.apiBaseUrl() + "/" + this.apiModel() + "/" + this.GET + model.id, headers)
            .map(this.extractData)
            .catch(this.handleError);
    };
    /**
     * @return {Observable<R>}
     */
    BaseApiService.prototype.getAll = function () {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return this.http.get(this.apiBaseUrl() + "/" + this.apiModel() + "/" + this.GET, headers)
            .map(this.extractData)
            .catch(this.handleError);
    };
    /**
     * Creates a new model and saves it
     * @param model
     * @return {Observable<R>}
     */
    BaseApiService.prototype.create = function (model) {
        this.beforeCreate();
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers });
        var body = JSON.stringify(model);
        return this.http.post(this.apiBaseUrl() + "/" + this.apiModel() + "/" + this.CREATE, body, headers)
            .map(this.extractData)
            .catch(this.handleError);
    };
    /**
     * Updates model by model.id and its params
     * @param model
     * @return {Observable<R>}
     */
    BaseApiService.prototype.update = function (model) {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers });
        var body = JSON.stringify(model);
        return this.http.put(this.apiBaseUrl() + "/" + this.apiModel() + "/" + this.UPDATE + model.id, body, headers)
            .map(this.extractData)
            .catch(this.handleError);
    };
    /**
     * Deletes model by model.id
     * @param model
     * @return {Observable<Response>}
     */
    BaseApiService.prototype.remove = function (model) {
        if (!model.id) {
            return;
        }
        return this.http.delete(this.apiBaseUrl() + "/" + this.apiModel() + "/" + this.DELETE + model.id)
            .map(this.extractData)
            .catch(this.handleError);
    };
    /**
     *
     * @param res
     * @return {{}}
     */
    BaseApiService.prototype.extractData = function (res) {
        var body = res.json();
        return this.onResponse(body.data || {});
    };
    /**
     * @param error
     * @return {any}
     */
    BaseApiService.prototype.handleError = function (error) {
        // In a real world app, we might use a remote logging infrastructure
        var errMsg;
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error('error', errMsg);
        return rx_1.Observable.throw(errMsg);
    };
    /**
     * Events
     */
    BaseApiService.prototype.beforeCreate = function () { };
    ;
    BaseApiService.prototype.beforeUpdate = function () { };
    ;
    BaseApiService.prototype.beforeRemove = function () { };
    ;
    BaseApiService.prototype.beforeGetAll = function () { };
    ;
    BaseApiService.prototype.beforeGetById = function () { };
    ;
    BaseApiService.prototype.afterCreate = function () { };
    ;
    BaseApiService.prototype.afterUpdate = function () { };
    ;
    BaseApiService.prototype.afterRemove = function () { };
    ;
    BaseApiService.prototype.afterGetAll = function () { };
    ;
    BaseApiService.prototype.afterGetById = function () { };
    ;
    BaseApiService.prototype.onError = function () { };
    ;
    BaseApiService = __decorate([
        __param(0, core_1.Inject(http_1.Http))
    ], BaseApiService);
    return BaseApiService;
}());
exports.BaseApiService = BaseApiService;
//# sourceMappingURL=base.api.service.js.map