///<reference path="../../../node_modules/rxjs/add/operator/map.d.ts"/>
///<reference path="../../interfaces/baseService.interface.ts"/>
/**
 * Created by tomislavfabeta on 12/2/17.
 */

import {Headers, Response, Http} from '@angular/http';
import {Observable} from 'rxjs/rx';
import 'rxjs/add/operator/map';
import {Inject} from "@angular/core";

export abstract class BaseApiService implements BaseServiceInterface{
    /**
     *
     * @type {string}
     */
    protected CREATE = '';
    protected GET = '';
    protected UPDATE = '';
    protected DELETE = '';


    /**
     * return {string} For example return 'user'
     */
    abstract apiModel(): string;
    protected abstract onResponse(response: Response): any;
    /**
     * Extend this method to change api version
     * @return {string}
     */
    protected apiVersion(){
        return 'v1/'
    }

    /**
     * Extend this method to change base api url
     * @return {string}
     */
    protected apiBaseUrl(){
        return `some.api.com/${this.apiVersion()}`;
    }

    /**
     * @param http
     */
    constructor(@Inject(Http) private http: Http) { }

    /**
     *
     * @param model
     * @return {Observable<R>}
     */
    getById(model) {
        console.log(model);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.get(`${this.apiBaseUrl()}/${this.apiModel()}/${this.GET}${model.id}`, headers)
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     * @return {Observable<R>}
     */
    getAll(){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return this.http.get(`${this.apiBaseUrl()}/${this.apiModel()}/${this.GET}`, headers)
            .map(this.extractData)
            .catch(this.handleError);
    }
    /**
     * Creates a new model and saves it
     * @param model
     * @return {Observable<R>}
     */
    create(model) {
        this.beforeCreate();
        let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers });
        let body = JSON.stringify(model);
        return this.http.post(`${this.apiBaseUrl()}/${this.apiModel()}/${this.CREATE}`, body, headers)
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     * Updates model by model.id and its params
     * @param model
     * @return {Observable<R>}
     */
    update(model) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        // let options = new RequestOptions({ headers: headers });
        let body = JSON.stringify(model);
        return this.http.put(`${this.apiBaseUrl()}/${this.apiModel()}/${this.UPDATE}${model.id}`, body, headers)
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     * Deletes model by model.id
     * @param model
     * @return {Observable<Response>}
     */
    remove(model) {
        if (!model.id) {
            return;
        }
        return this.http.delete(`${this.apiBaseUrl()}/${this.apiModel()}/${this.DELETE}${model.id}`)
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     *
     * @param res
     * @return {{}}
     */
    private extractData(res: Response) {
        let body = res.json();
        return this.onResponse(body.data || { });
    }

    /**
     * @param error
     * @return {any}
     */
    protected handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error('error',errMsg);
        return Observable.throw(errMsg);
    }

    /**
     * Events
     */
    protected beforeCreate() {};
    protected beforeUpdate() {};
    protected beforeRemove() {};
    protected beforeGetAll() {};
    protected beforeGetById() {};
    protected afterCreate() {};
    protected afterUpdate() {};
    protected afterRemove() {};
    protected afterGetAll() {};
    protected afterGetById() {};
    protected onError() {};
}
