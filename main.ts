/**
 * Created by tomislavfabeta on 12/2/17.
 */
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {AppModule} from "./app/app.module";

platformBrowserDynamic().bootstrapModule(AppModule);
